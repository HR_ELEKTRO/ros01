#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <ti/sysbios/BIOS.h>
#include "ti_drivers_config.h"

/* This program shows a problem that can arise if two threads operate on
 * the same shared variable. The problem can be fixed by using a mutex. */

#define SYNCHRONIZE 0 // Show problem
//#define SYNCHRONIZE 1 // Fix problem

void check(int error)
{
    if (error != 0)
    {
        printf("Error: %s\n", strerror(error));
        while (1);
    }
}

int count = 0;
#if SYNCHRONIZE==1
pthread_mutex_t m;
#endif

void *counter1(void *par)
{
    for (int i = 0; i < 500000; i++)
    {
#if SYNCHRONIZE==1
        check( pthread_mutex_lock(&m) );
#endif
        count += 1;
#if SYNCHRONIZE==1
        check( pthread_mutex_unlock(&m) );
#endif
    }
    return NULL;
}

void *counter2(void *par)
{
    struct timespec delay = {.tv_sec = 0, .tv_nsec = 1};
    for (int i = 0; i < 500; i++)
    {
#if SYNCHRONIZE==1
        check( pthread_mutex_lock(&m) );
#endif
        count += 1;
#if SYNCHRONIZE==1
        check( pthread_mutex_unlock(&m) );
#endif
        clock_nanosleep(CLOCK_MONOTONIC, 0, &delay, NULL);
    }
    return NULL;
}

void *main_thread(void *arg)
{
#if SYNCHRONIZE==1
    pthread_mutexattr_t ma;
    check( pthread_mutexattr_init(&ma) );
    check( pthread_mutex_init(&m, &ma) );
#endif

    pthread_attr_t pta1;
    check( pthread_attr_init(&pta1) );
    check( pthread_attr_setstacksize(&pta1, 1024) );
    struct sched_param sp1;
    check( pthread_attr_getschedparam(&pta1, &sp1) );
    sp1.sched_priority = 1;
    check( pthread_attr_setschedparam(&pta1, &sp1) );

    pthread_attr_t pta2;
    check( pthread_attr_init(&pta2) );
    check( pthread_attr_setstacksize(&pta2, 1024) );
    struct sched_param sp2;
    check( pthread_attr_getschedparam(&pta2, &sp2) );
    sp2.sched_priority = 2;
    check( pthread_attr_setschedparam(&pta2, &sp2) );

    pthread_t t1, t2;
    check( pthread_create(&t1, &pta1, &counter1, NULL) );
    check( pthread_create(&t2, &pta2, &counter2, NULL) );

    check( pthread_join(t1, NULL) );
    check( pthread_join(t2, NULL) );
    printf("\ncount = %d\n", count);

#if SYNCHRONIZE==1
    check( pthread_mutexattr_destroy(&ma) );
    check( pthread_mutex_destroy(&m) );
#endif

    check( pthread_attr_destroy(&pta1) );
    check( pthread_attr_destroy(&pta2) );

    return NULL;
}

int main(void)
{
    Board_init();

    pthread_attr_t pta;
    check( pthread_attr_init(&pta) );
    check( pthread_attr_setdetachstate(&pta, PTHREAD_CREATE_DETACHED) );
    check( pthread_attr_setstacksize(&pta, 2048) );

    struct sched_param sp;
    check( pthread_attr_getschedparam(&pta, &sp) );
    // The main thread must have the highest priority because this thread will start
    // the other threads and we want to study the interaction between those other threads
    sp.sched_priority = 15;
    check( pthread_attr_setschedparam(&pta, &sp) );

    pthread_t pt;
    check( pthread_create(&pt, &pta, main_thread, NULL) );

    printf("\n");
    BIOS_start();

    check( pthread_attr_destroy(&pta) );

    return EXIT_SUCCESS;
}
