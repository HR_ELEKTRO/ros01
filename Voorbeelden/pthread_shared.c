#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <ti/sysbios/BIOS.h>
#include "ti_drivers_config.h"

/* This program shows that threads with equal priority are not alternated in TI-RTOS.
 * Take a look at mutex.c to see a demonstration of a synchronization problem
 * between theads with different priorities. */

void check(int error)
{
    if (error != 0)
    {
        printf("Error: %s\n", strerror(error));
        while (1);
    }
}

volatile int aantal = 0;

void *teller(void *par)
{
    for (int i = 0; i < 100; i++)
    {
        printf("%d\n", *(int *)par);
        aantal++;
    }
    return NULL;
}

void *main_thread(void *arg)
{
    pthread_attr_t pta;
    check( pthread_attr_init(&pta) );
    check( pthread_attr_setstacksize(&pta, 1024) );

    pthread_t t1, t2, t3;
    int pid[3] = {1, 2, 3};
    check( pthread_create(&t1, &pta, &teller, &pid[0]) );
    check( pthread_create(&t2, &pta, &teller, &pid[1]) );
    check( pthread_create(&t3, &pta, &teller, &pid[2]) );

    check( pthread_join(t1, NULL) );
    check( pthread_join(t2, NULL) );
    check( pthread_join(t3, NULL) );

    printf("aantal = %d\n", aantal);

    check( pthread_attr_destroy(&pta) );

    return NULL;
}

int main(void)
{
    Board_init();

    pthread_attr_t pta;
    check( pthread_attr_init(&pta) );
    check( pthread_attr_setdetachstate(&pta, PTHREAD_CREATE_DETACHED) );
    check( pthread_attr_setstacksize(&pta, 2048) );

    struct sched_param sp;
    check( pthread_attr_getschedparam(&pta, &sp) );
    // The main thread must have the highest priority because this thread will start
    // the other threads and we want to study the interaction between those other threads
    sp.sched_priority = 15;
    check( pthread_attr_setschedparam(&pta, &sp) );

    pthread_t pt;
    check( pthread_create(&pt, &pta, main_thread, NULL) );

    printf("\n");
    BIOS_start();

    check( pthread_attr_destroy(&pta) );

    return EXIT_SUCCESS;
}
