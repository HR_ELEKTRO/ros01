#include <errno.h>
#include <semaphore.h>
#include <xdc/runtime/Assert.h>
#include <ti/sysbios/knl/Semaphore.h>

// Implementation of sem_init which uses a priority based waiting queue
int sem_priority_init(sem_t *semaphore, int pshared, unsigned value)
{
    typedef struct {
        Semaphore_Struct sem;
    } sem_obj;
    sem_obj *obj = (sem_obj*)(&semaphore->sysbios);

    /* object size validation */
    Assert_isTrue(sizeof(sem_obj) <= sizeof(sem_t), NULL);

    if (value > 65535) {
        errno = EINVAL;
        return -1;
    }

    /* semaphore mode is Semaphore_Mode_COUNTING_PRIORITY */
    Semaphore_Params sem_pars;
    Semaphore_Params_init(&sem_pars);
    sem_pars.mode = Semaphore_Mode_COUNTING_PRIORITY;
    Semaphore_construct(&(obj->sem), (int)value, &sem_pars);
    return 0;
}
