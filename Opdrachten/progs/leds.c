/*
 * Copyright (c) 2020, Rotterdam University of Applied Sciences
 * All rights reserved.
 */

#include <stdint.h>
#include <stddef.h>

/* Driver Header files */
#include <NoRTOS.h>
#include <ti/drivers/GPIO.h>

/* Driver configuration */
#include "ti_drivers_config.h"

int main(void)
{
    Board_init();
    NoRTOS_start();

    /* Call driver init functions */
    GPIO_init();
    /* Turn on red led */
    GPIO_write(CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_ON);
    /* Turn off green and yellow led */
    GPIO_write(CONFIG_GPIO_LED_1, CONFIG_GPIO_LED_OFF);
    GPIO_write(CONFIG_GPIO_LED_2, CONFIG_GPIO_LED_OFF);

    while (1) {} /* Wait forever */
}
